package com.demo.fcm_demo;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by sachinsharma on 23/1/17.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static String TAG = "MyFirebaseIIDService";
    private String refreshedToken;

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token : " + refreshedToken);

        SharedPreferences.Editor editor = getSharedPreferences("FCMDemoSharedPref", MODE_PRIVATE).edit();
        editor.putString("refreshedToken", refreshedToken);
        editor.commit();


        sendRegistrationToServer(refreshedToken);
        }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
